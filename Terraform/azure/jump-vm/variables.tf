variable "username" {
  type        = string
  description = "The username for the local account that will be created on the new VM."
}

variable "admin_password" {
  type = string
  description = "Admin password for the virtual machine"
}
