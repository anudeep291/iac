output "resource_group_name" {
  value = module.jump-vm.resource_group_name
}

output "public_ip_address" {
  value = module.jump-vm.public_ip_address
}
