resource "azurerm_dns_zone" "dnszone" {
  name                = var.domain_name
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_dns_a_record" "dnsarec" {
  name                = ""
  zone_name           = azurerm_dns_zone.dnszone.name
  resource_group_name = azurerm_resource_group.rg.name
  ttl                 = 3600
  records             = [azurerm_linux_virtual_machine.my_terraform_vm.public_ip_address]
}
